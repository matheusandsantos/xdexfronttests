var conf = require('../../nightwatch.conf.js');
const defaultWaitTime = 4000; 
module.exports = {
    'Demo backoffice': function (browser) {
        browser
            .url('http://local.azurewebsites.net/backoffice/') // visit the url
            .waitForElementVisible('a.alert-link', defaultWaitTime, true, ()=>null, 'Esperando tela do azure carregar'); // wait for the body to be rendered

        browser
            .click('a.alert-link')
            .waitForElementVisible('input[type="email"]', defaultWaitTime, true, ()=>null, 'Buscando campo de e-mail')
            .setValue('input[type="email"]', 'george.t0435.sciensa@xdex.com.br')
            .click('input[type="submit"]');
            
        browser
            .waitForElementVisible('input[type="password"]', defaultWaitTime, false, ()=>null, 'Buscando campo de password')
            .setValue('input[type="password"]','pha4aCh+')
            .click('#submitButton');

        browser
            .waitForElementNotPresent('#errorText', 500, true, ()=>null, 'PASSWORD ERROR');

        browser
            .waitForElementVisible('input[type="submit"]', defaultWaitTime, true, ()=>null, 'Buscando campo de continuar conectado')
            .click('input[type="submit"]');
        
        browser
            .waitForElementVisible('.customers-container', defaultWaitTime, true, ()=>null, 'Verificando se a tela do backoffice foi carregada')
            .end()
    }
};