const conf = require('../../../../nightwatch.conf.js');
const path = require('path');
const defaultWaitTime = 4000; 

module.exports = {
    'Teste pa': function (browser) {
        browser
            .url('http://local.azurewebsites.net/') // visit the url
            .waitForElementVisible('body', defaultWaitTime, true, ()=>null, 'Acessando landing page'); // wait for the body to be rendered
            
        let filePath = path.resolve(__dirname,'../../assets/pdf_example.pdf');

        browser
            .assert.elementPresent('a[href="#/carreiras"]','Encontrando link para tela de carreiras')
            .click('a[href="#/carreiras"]')
            .waitForElementVisible('body', defaultWaitTime, true, ()=>null, 'Acessando tela de carreiras')
            .setValue('input[name="name"]','Vitor Costa José')
            .setValue('input[name="email"]','vitor.jose@soaexpert.com.br')
            .setValue('input[name="phone"]','1199462-8799')
            .setValue('input[type="file"]',filePath)
            .click('select.area option[value="Comercial"]');
            // .click('select.how-hear-about-us option[value="Google"]');

        // browser.expect.element('button#sendForm').to.not.have.attribute('disabled');


        // browser.waitForElementNotPresent('.error-send');
        // browser.waitForElementVisible('.success-send');

        // browser.pause(3000);
        // browser.end();
            
    }
};