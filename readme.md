## Repositório criado para implementação de Testes de BDD para as aplicações do FRONT-END do projeto XDEX.

## PRÉ REQUISITO
JAVA
## segue link para instrução de como instalar no ubuntu 16.04
https://www.digitalocean.com/community/tutorials/como-instalar-o-java-com-apt-get-no-ubuntu-16-04-pt



## Como instalar e rodar este repo na sua máquina
1. npm install
2. npm test

## npm test executará o teste de todas as aplicações, para consultar quais aplicações são testaveis e como testar cada uma consulte a sessão "scripts"

## Ex. de como testar o uma aplicação em especifica
npm test --landing
or
npm test --trader

## Para adicionar uma nova aplicação aos testes adicione a pasta dentro da pasta test/e2e, liste essa pasta dentro de "scripts" no arquivo package.json e liste também na sessão "src_folders" no arquivo nightwatch.conf.js

